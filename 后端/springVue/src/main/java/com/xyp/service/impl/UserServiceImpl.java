package com.xyp.service.impl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xyp.mapper.UserMapper;
import com.xyp.pojo.Book;
import com.xyp.pojo.User;
import com.xyp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public User findUserNameAndPaw(String username, String password) {
        return userMapper.findUserNameAndPaw(username,password);
    }

    public List<User> findAll() {
        return userMapper.findAll();
    }
    public Map<String, Object> queryAll(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<User> list = userMapper.findAll();
        PageInfo pageInfo = new PageInfo(list);
        Map<String ,Object> map = new HashMap<>();
        //查询数据
        map.put("userList",pageInfo.getList());
        //查询总条数
        map.put("total",pageInfo.getTotal());
        return map;
    }
    @Override
    public User findUserName(String username) {
        return userMapper.findUserName(username);
    }

    @Override
    public User findById(Integer id) {
        return userMapper.findById(id);
    }

    @Override
    public int addUser(User user) {
        return userMapper.addUser(user);
    }

    @Override
    public int editUser(User user) {
        return userMapper.editUser(user);
    }

    @Override
    public int deleteUser(Integer id) {
        return userMapper.deleteUser(id);
    }

    @Override
    public int updateState(Integer id, Boolean state) {
        return userMapper.updateState(id,state);
    }
}
