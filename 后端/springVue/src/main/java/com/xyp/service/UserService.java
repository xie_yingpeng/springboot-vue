package com.xyp.service;

import com.xyp.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    public User findUserNameAndPaw(String username, String password);
    public List<User> findAll();
    public Map<String, Object> queryAll(Integer pageNum, Integer pageSize) ;
    public User findUserName(String username);
    public User findById(Integer id);
    public int addUser(User user);
    public int editUser(User user);
    public int deleteUser(Integer id);
    public int updateState(Integer id, Boolean state);
}
