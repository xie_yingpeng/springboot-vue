package com.xyp.service;

import com.xyp.pojo.Book;

import java.util.List;
import java.util.Map;

public interface BookService {
    List<Book> findAll();
    public Map<String ,Object> queryAll(Integer pageNum,Integer pageSize);
    //按照id查询
    Book findById(Integer id);
    //添加一个书籍
    int addBook(Book book);
    //修改
    int updateBook(Book book);
    //删除
    int deleteBook(Integer id);
    //根据书籍名查询书籍
    Book findByName(String bookName);
}
