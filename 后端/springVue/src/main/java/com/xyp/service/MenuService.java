package com.xyp.service;

import com.xyp.pojo.MainMenu;

import java.util.List;

public interface MenuService {
    public List<MainMenu> getMainMenus();
}
