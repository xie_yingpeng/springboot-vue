package com.xyp.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xyp.mapper.BookMapper;
import com.xyp.pojo.Book;
import com.xyp.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookMapper bookMapper;
    @Override
    public List<Book> findAll() {
        return bookMapper.findAll();
    }

    @Override
    public Map<String, Object> queryAll(Integer pageNum,Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Book> list = bookMapper.findAll();
        PageInfo pageInfo = new PageInfo(list);
        Map<String ,Object> map = new HashMap<>();
        //查询数据
        map.put("bookList",pageInfo.getList());
        //查询总条数
        map.put("total",pageInfo.getTotal());
        return map;
    }

    @Override
    public Book findById(Integer id) {
        return bookMapper.findById(id);
    }

    @Override
    public int addBook(Book book) {
        return bookMapper.addBook(book);
    }

    @Override
    public int updateBook(Book book) {
        return bookMapper.updateBook(book);
    }

    @Override
    public int deleteBook(Integer id) {
        return bookMapper.deleteBook(id);
    }

    @Override
    public Book findByName(String bookName) {
        return bookMapper.findByName(bookName);
    }
}
