package com.xyp.service.impl;

import com.xyp.mapper.MenMapper;
import com.xyp.pojo.MainMenu;
import com.xyp.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenMapper mapper;
    @Override
    public List<MainMenu> getMainMenus() {
        return mapper.getMainMenus();
    }
}
