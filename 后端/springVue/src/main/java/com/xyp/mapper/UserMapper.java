package com.xyp.mapper;
import com.xyp.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface UserMapper {
    public User findUserNameAndPaw(String username,String password);
    public List<User> findAll();
    public User findUserName(String username);
    public User findById(Integer id);
    public int addUser(User user);
    public int editUser(User user);
    public int deleteUser(Integer id);
    public int updateState(Integer id, Boolean state);
}
