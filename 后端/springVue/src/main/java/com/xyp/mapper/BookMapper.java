package com.xyp.mapper;

import com.xyp.pojo.Book;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface BookMapper {
    List<Book> findAll();
    //按照id查询
    Book findById(Integer id);
    //添加一个书籍
    int addBook(Book book);
    //修改
    int updateBook(Book book);
    //删除
    int deleteBook(Integer id);
    //根据书籍名查询书籍
    Book findByName(String bookName);
}
