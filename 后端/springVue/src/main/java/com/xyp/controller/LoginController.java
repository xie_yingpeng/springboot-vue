package com.xyp.controller;

import com.xyp.pojo.User;
import com.xyp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {
    @Autowired
    private UserService userService;

    @CrossOrigin
    @RequestMapping("/login")
    @ResponseBody
    public String login(@RequestBody User user){
        User user1 = userService.findUserNameAndPaw(user.getUsername(),user.getPassword());
        if(user1!=null){
            return "success";
        }
        return "error";
    }
}
