package com.xyp.controller;
import com.xyp.pojo.Book;
import com.xyp.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/book")
public class BookController {
    @Autowired
    private BookService bookService;
    @RequestMapping("/findAll")
    @ResponseBody
    public List<Book> findAll(){
        return bookService.findAll();
    }
    @RequestMapping("/queryAll/{pageNum}")
    @ResponseBody
    public Map<String,Object> queryAll(@PathVariable("pageNum") Integer pageNum,Integer pageSize){
        return bookService.queryAll(pageNum,5);
    }
    @RequestMapping(value = "/addBook")
    @ResponseBody
    public String addBook(Book book){
        int n =bookService.addBook(book);
        if(n>0){
            return "success";
        } else {
            return "error";
        }

    }
    @RequestMapping(value = "/deleteBook/{id}")
    @ResponseBody
    public String deleteBook(@PathVariable("id") Integer id){
        int n =bookService.deleteBook(id);
        if(n>0){
            return "success";
        } else {
            return "error";
        }

    }
}
