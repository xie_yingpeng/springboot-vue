package com.xyp.controller;
import com.xyp.pojo.User;
import com.xyp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/getUpdate")
    @ResponseBody
    public User getUpdate(@RequestParam("id") Integer id){
        return userService.findById(id);
    }
    @RequestMapping("/queryAll")
    @ResponseBody
    public Map<String,Object> queryAll(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize){

        return userService.queryAll(pageNum,pageSize);
    }

    @RequestMapping("/updateState")
    @ResponseBody
    public String updateState(@RequestParam("id") Integer id,@RequestParam("state") boolean state){
        int n = userService.updateState(id,state);
        if(n>0){
            return "success";
        }
        return "error";
    }
    @RequestMapping("/addUser")
    @ResponseBody
    public String addUser(@RequestBody User user){
        int n = userService.addUser(user);
        if(n>0){
            return "success";
        }
        return "error";
    }
    @RequestMapping("/deleteUser")
    @ResponseBody
    public String deleteUser(@RequestParam("id") Integer id){
        int n = userService.deleteUser(id);
        if(n>0){
            return "success";
        }
        return "error";
    }
    @RequestMapping("/editUser")
    @ResponseBody
    public String editUser(@RequestBody User user){
        int n = userService.editUser(user);
        if(n>0){
            return "success";
        }
        return "error";
    }
}
