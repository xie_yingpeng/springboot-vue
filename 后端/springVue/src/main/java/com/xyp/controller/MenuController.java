package com.xyp.controller;

import com.xyp.pojo.MainMenu;
import com.xyp.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
public class MenuController {
        @Autowired
        private MenuService menuService;
        @RequestMapping("/menus")
        @ResponseBody
        public Object getAllMenus(){
            HashMap<String, Object> data = new HashMap<>();
            List<MainMenu> mainMenus = menuService.getMainMenus();
            data.put("data",mainMenus);
            data.put("status",200);
            return data;
        }
}
