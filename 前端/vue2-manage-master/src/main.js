import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import axios from 'axios'
import qs from 'qs'
Vue.prototype.$qs = qs
// 挂载axios 到Vue的原型prototype的$http
Vue.prototype.$http = axios
// 设置请求的根路径
axios.defaults.baseURL = "http://localhost:8088/"
Vue.config.productionTip = false;
Vue.use(ElementUI);

axios.interceptors.request.use(config => {
	console.log(config);
	// 请求头挂载信息
	config.headers.Authorization = window.sessionStorage.getItem("flag");
	// 在最后必须return config
	return config;
  })

new Vue({
	el: '#app',
	router,
	store,
	template: '<App/>',
	components: { App }
})
